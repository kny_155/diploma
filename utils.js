function onReady(text) {
    document.getElementById(text).innerHTML = 'Загружен';
}

let addFileInputHandler = (fileInputId, canvasId) => {
    let inputElement = document.getElementById(fileInputId);
    inputElement.addEventListener('change', (e) => {
        let files = e.target.files;
        if (files.length > 0) {
            let imgUrl = URL.createObjectURL(files[0]);
            loadImageToCanvas(imgUrl, canvasId);
        }
    }, false);
};

let loadImageToCanvas = (url, cavansId) => {
    let canvas = document.getElementById(cavansId);
    let ctx = canvas.getContext('2d');
    let img = new Image();
    img.crossOrigin = 'anonymous';
    img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
    };
    img.src = url;
};




addFileInputHandler('fileInput', "canvasInput");